/* This takes care of all the file I/O operations. */
package service

import (
	"bitbucket.org/pers3us/todo.txt/common"
	"github.com/dlintw/goconf"
	"io/ioutil"
	"log"
	"os"
	"strings"
)

var C *common.Conf
var tasks []common.Task

// CONF Operation
func value(key string, conf string) string {
	c, err := goconf.ReadConfigFile(conf)
	common.Fatal(err)
	val, err := c.GetRawString("default", key) // hardcoding defautl. TODO: change the hardcoding later
	common.Fatal(err)
	return val
}

//FILE I/O

//Read from files
func readFile() {
	tasks = nil
	content, err := ioutil.ReadFile(C.Path + common.Slash() + "todo.txt")
	common.Fatal(err)
	lines := strings.Split(string(content), "\n")
	for k, value := range lines {
		if value != "" {
			params := strings.Split(value, " ")
			var task common.Task
			task.Id = k
			for _, v := range params {
				if check := strings.ContainsAny(v, "x"); check == true {
					task.Done = true
				} else if check := strings.ContainsAny(v, "( & )"); check == true { // check for priority first
					task.Priority = v
				} else if check := strings.ContainsAny(v, "@"); check == true { // check for @
					task.Tags = append(task.Tags, v)
				} else if check := strings.ContainsAny(v, "+"); check == true { // check for +
					task.Project = append(task.Project, v)
				} else {
					task.Task += v + " "
				}
			}
			task.Task = strings.TrimRight(task.Task, " ")
			tasks = append(tasks, task)
		}
	}
}

func writeFile() {
	str := ""
	for _, v := range tasks {
		if v.Done {
			str += "x "
		}
		if v.Priority != "" {
			str += v.Priority + " "
		}
		str += v.Task
		for _, val := range v.Tags {
			if val != "" {
				str += " " + val
			}

		}
		for _, val := range v.Project {
			if val != "" {
				str += " " + val
			}
		}
		str += "\n"
	}
	ioutil.WriteFile(C.Path+common.Slash()+"todo.txt", []byte(str), os.ModeExclusive)
}

func isNew(list []string, key string) bool {
	for _, value := range list {
		if value == key {
			return false
		}
	}
	return true
}

//Add task to task[]
func addToTask(t string) {
	log.Print(t)
	params := strings.Split(t, " ")
	var task common.Task
	for _, v := range params {
		if check := strings.ContainsAny(v, "( & )"); check == true { // check for priority first
			task.Priority = v
			log.Print(task.Priority)
		} else if check := strings.ContainsAny(v, "@"); check == true { // check for @
			task.Tags = append(task.Tags, v)
		} else if check := strings.ContainsAny(v, "+"); check == true { // check for +
			task.Project = append(task.Project, v)
		} else {
			task.Task += v + " "
		}
	}
	task.Task = strings.TrimRight(task.Task, " ")
	tasks = append(tasks, task)
}

//Remove task from task[]
func delete(i int64) {
	switch i {
	case 0:
		tasks = tasks[1:]
	case int64(len(tasks) - 1):
		tasks = tasks[:len(tasks)-1]
	default:
		tasks = append(tasks[:i], tasks[i+1:]...)
	}
}
