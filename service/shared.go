package service

import (
	"bitbucket.org/pers3us/todo.txt/common"
	"encoding/json"
	"strconv"
)

// Set CONF
func SetC(path string) {
	C = &common.Conf{
		Port: value("port", path),
		Path: value("filepath", path),
	}
}

func ReadFile() {
	readFile()
}

// DISPLAY FUNCTIONS

//List Tasks
func Task() []byte {
	var notdone []common.Task
	for _, v := range tasks {
		if !v.Done {
			notdone = append(notdone, v)
		}
	}
	type TaskList struct {
		Tasks []common.Task
	}
	t := TaskList{
		Tasks: notdone,
	}
	buff, err := json.Marshal(t)
	common.Panic(err)
	return buff
}

//List the tasks which are done
func Done() []byte {
	var done []common.Task
	for _, v := range tasks {
		if v.Done {
			done = append(done, v)
		}
	}
	type TaskList struct {
		Done []common.Task
	}
	t := TaskList{
		Done: done,
	}
	buff, err := json.Marshal(t)
	common.Panic(err)
	return buff
}

// List Tags
func Tags() []byte {
	type TagList struct {
		Tags []string
	}
	var tags []string
	//TODO: Change the logic at a later point
	for _, v := range tasks {
		if v.Tags != nil {
			for _, value := range v.Tags {
				if isNew(tags, value) {
					tags = append(tags, value)
				}
			}
		}
	}
	t := TagList{
		Tags: tags,
	}
	buff, err := json.Marshal(t)
	common.Panic(err)
	return buff
}

// List Projects
func Projects() []byte {
	type ProjectList struct {
		Projects []string
	}
	var projects []string
	//TODO: Change this logic at a later time.
	for _, v := range tasks {
		if v.Project != nil {
			for _, value := range v.Project {
				if isNew(projects, value) {
					projects = append(projects, value)
				}
			}
		}
	}
	t := ProjectList{
		Projects: projects,
	}
	buff, err := json.Marshal(t)
	common.Panic(err)
	return buff
}

//OPERATIONS

//Add task
func Add(task string) bool {
	if task == "" {
		return false
	}
	addToTask(task)
	writeFile()
	readFile()
	return true
}

// Mark a task with task id as done.
func Do(i string) bool {
	taskid, err := strconv.ParseInt(i, 10, 0)
	common.Panic(err)
	// move the done task to done array
	tasks[taskid].Done = true
	writeFile()
	readFile()
	return true
}

// Mark a task with task id as done.
func Restore(i string) bool {
	taskid, err := strconv.ParseInt(i, 10, 0)
	common.Panic(err)
	// move the done task to done array
	tasks[taskid].Done = false
	writeFile()
	readFile()
	return true
}

func Sync() bool {
	readFile()
	return true
}

func Project(name string) []byte{
	var task []common.Task
	for _, v := range tasks {
		for _,val := range v.Project{
			if(val == name){
				task = append(task, v)
				break
			}	
		}
	}
	type TaskList struct {
		Tasks []common.Task
	}
	t := TaskList{
		Tasks: task,
	}
	buff, err := json.Marshal(t)
	common.Panic(err)
	return buff
}
