package common

import (
	"log"
	"runtime"
)

func Slash() string {
	slash := "/"
	if runtime.GOOS == "windows" {
		slash = "\\"
	}
	return slash
}

type Conf struct {
	Port string
	Path string
}

type Task struct {
	Id       int
	Priority string
	Task     string
	Tags     []string
	Project  []string
	Done     bool
}

func Fatal(err error) {
	if err != nil {
		log.Fatal(err.Error())
	}
}

func Panic(err error) {
	if err != nil {
		log.Fatal(err.Error())
	}
}
