package main

import (
	"bitbucket.org/pers3us/todo.txt/common"
	"bitbucket.org/pers3us/todo.txt/network"
	"bitbucket.org/pers3us/todo.txt/service"
	"io"
	"log"
	"os"
)

var c *common.Conf

func main() {
	//set the logger detail.

	// get port number to run from the config file.
	pwd, err := os.Getwd()
	common.Fatal(err)

	// TODO: Use filepath to make it work in windows.
	// TODO: Fix here and use OpenFile with access specifier.
	fo, err := os.Create(pwd + common.Slash() + "todo.txt.log")
	common.Fatal(err)            // log file
	log.SetOutput(io.Writer(fo)) // log now writes to file defined above.

	//check if config file exits
	//TODO: Later use the file/Filepath to make it work in any OS.
	path := pwd + common.Slash() + ".conf" // path

	// get the port number and path and set in the sturcture from conf file.
	service.SetC(path) // sets the value in the service.
	service.ReadFile()
	network.Server(service.C.Port)

}
