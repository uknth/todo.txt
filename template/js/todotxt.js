      function sync(){
        var posting = $.post("/sync");
        posting.done(function( data ) {
          if (data == "OK"){
            notify("success","Sync Complete")
          }else if (data == "ERROR"){
          }
          getTasks();
        });
      }
      function getTasks(){
        $('.nav li').removeClass('active');
        $('#header').html("Tasks");
        $('#content').empty();
        $('#nav-task').addClass('active');
        $.ajax({
          url: "/tasks",
          dataType: "json"
        }).done(function(data) {
          var str='';
          if(data.Tasks != null){
            $.each(data.Tasks,function(k,v){
              str += '<tr id=\"'+v.Id+'\" data-id = \"'+v.Id+'\" ondblclick=\"javascript:donewithtask('+v.Id+')\" class=\"task-text '+v.Priority+'\"><td><span class=\"pull-left\">'+v.Task + '</td><td>';
              if(v.Tags!=null && v.Tags.length>0){
                $.each(v.Tags,function(k,v){
                  str += '<span class=\"badge badge-info pull-right pad-left\"><small><a  href=\"/tag/'+ v +'\" class=\"label-text\">' + v + '</a></small></span>';
                });  
              }
              if(v.Project!=null && v.Project.length>0 ){
                $.each(v.Project,function(k,v){
                  str += ' <span class=\"badge badge-inverse pull-right pad-left\"><small><a  href=\"/tag/'+ v +'\" class=\"label-text\">' + v + '</a></small></span>';
                });
              }
              str += '&nbsp;</td></tr>';
            });
          }
          if (str == ''){
            str = 'Hurray! No tasks!';
          }
          $('#content').append(str);
        });
      }
      function donewithtask(id){
        done(id);
        getTasks();
      }
      function restorewithtask(id){
        restore(id);
        getDone();
      }
      function getTags(){
        $('.nav li').removeClass('active');
        $('#header').html("Tags");
        $('#content').empty();
        $('#nav-tags').addClass('active');
        $.ajax({
          url: "/tags",
          dataType: "json"
        }).done(function(data) {
          var str = '';var count = 0;
          if (data.Tags != null){
            str += '<tr>';
            $.each(data.Tags,function(k,v){
              if (count<5){
                str += '<td><span class=\"badge badge-success\">' + v + '</span></td>';                
                count ++;
              }else{
                str += '</tr><tr>';
                count =0;
              }
               
            });
            str += '</tr>';  
          }else {
            str = 'No Tags found';
          }
          $('#content').append(str);
        });
      }
      function getProjects(){        
        $.ajax({
          url: "/projects",
          dataType: "json"
        }).done(function(data){
          var str = '';
          if (data.Projects != null){
            $.each(data.Projects,function(k,v){
	      str += '<li><a href = "#" onclick="tasksWithProject(\''+v+'\');">' + v + '</a></li>'; 
            });  
          }else {
            str = 'No Projects found';
          }
          $('#projects-dropdown').append(str);
        });
      }
      function restorewithproj(id,name){
        restore(id);
        tasksWithProject(name);
      }
      function donewithproj(id,name){
        done(id);
        tasksWithProject(name); 
      }
      function tasksWithProject(name){
        $('#header').html("Tasks With Project &nbsp;&nbsp;&nbsp;<span class=\"label label-inverse\">"+name+"</span>");
        $('#content').empty();
        $.ajax({
          type: "POST",
          url: "/project",
          dataType: "json",
          data: { project: name},
        }).done(function(data){
          //Populate table
          var str='';
          if(data.Tasks != null){
            $.each(data.Tasks,function(k,v){
              var done = '';var func = '';
              if(v.Done){
                done = '<span class = \"label label-warning pull-right mar-left\">D</span>';
                func = 'ondblclick=\"javascript:restorewithproj('+v.Id+',\''+name+'\')\"';
              }else{
                func = 'ondblclick=\"javascript:donewithproj('+v.Id+',\''+name+'\')\"';
              }
              str += '<tr id=\"'+v.Id+'\" data-id = \"'+v.Id+'\" '+func+' class=\"task-text  '+v.Priority+'\"><td><span class=\"pull-left\">'+v.Task + '</span></td><td>';
              if(v.Tags!=null && v.Tags.length>0){
                $.each(v.Tags,function(k,v){
                  str += '<span class=\"badge badge-info pull-right pad-left\"><small><a  href=\"/tag/'+ v +'\" class=\"label-text\">' + v + '</a></small></span>';
                });  
              }
              if(v.Project!=null && v.Project.length>0 ){
                $.each(v.Project,function(k,v){
                  str += ' <span class=\"badge badge-inverse pull-right pad-left\"><small><a  href=\"/tag/'+ v +'\" class=\"label-text\">' + v + '</a></small></span>';
                });
              }
              if (done != ''){
                str += done;
              }
              str += '&nbsp;</td></tr>';
            });
          }
          if (str == ''){
            str = 'Hurray! No tasks!';
          }
          $('#content').append(str);
        });
      }
      $('#add-task').submit(function(){
        $('#add-task-input').attr("placeholder","Add Task");
        event.preventDefault();
        var $form = $(this),
          term = $form.find( 'input[name="task"]' ).val(),
          url = $form.attr( 'action' );
        var posting = $.post( url, { task: term } );
        $('#add-task-input').val('');
        posting.done(function( data ) {
          if (data == "OK"){
            notify("success","Task added")
          }else if (data == "ERROR"){
          }
          getTasks();
        });
      });
      function done(number){
        var posting = $.post("/do",{id : number});
        posting.done(function(data){
          if (data == "OK"){
            notify("success","Task marked as done")
          }else{
            notify("error","Couldn't mark the task as done")
          }
        });

      }
      function restore(number){
        var posting = $.post("/restore",{id:number});
        posting.done(function(data){
          if (data == "OK"){
            notify("success","Task restored")
          }else{
            notify("error","Couldn't restore the task")
          }
        });
      }

      function getDone(){
        $('.nav li').removeClass('active');
        $('#header').html("Done");
        $('#content').empty();
        $('#nav-done').addClass('active');  
        $.ajax({
          url: "/done",
          dataType: "json"
        }).done(function(data) {
          var str='';
            $.each(data.Done,function(k,v){
            str += '<tr id=\"'+v.Id+'\" data-id = \"'+v.Id+'\" ondblclick=\"javascript:restorewithtask('+v.Id+')\" class=\"task-text '+v.Priority+'\"><td><span class=\"pull-left\">'+v.Task + '</td><td>';
            if(v.Tags!=null && v.Tags.length>0){
              $.each(v.Tags,function(k,v){
                str += '<span class=\"badge badge-info pull-right pad-left\"><small><a  href=\"/tag/'+ v +'\" class=\"label-text\">' + v + '</a></small></span>';
              });  
            }
            if(v.Project!=null && v.Project.length>0 ){
              $.each(v.Project,function(k,v){
                str += ' <span class=\"badge badge-inverse pull-right pad-left\"><small><a  href=\"/tag/'+ v +'\" class=\"label-text\">' + v + '</a></small></span>';
              });
            }
            str += '<span class = \"label label-warning pull-right mar-left\">D</span>&nbsp;</td></tr>';
          });
          if (str == ''){
            str = 'Sad! No -DONE- tasks!';
          }
          $('#content').append(str);
        });
      }

      function ClickToClose(){
        $("#notification-div").addClass("hide");
      }
      function notify(type,message){
        $("#notification-div").delay(0).fadeIn(500);
        switch(type){
          case "info":  $("#notification-div").addClass("alert-info");          break;
          case "error": $("#notification-div").addClass("alert-error");          break;
          case "success": $("#notification-div").addClass("alert-success");          break;
          default:
            $("notification-div").addClass("alert-block")
        }
        $("#notification-div").removeClass("hide")
        $("#notification").text(message);
        $("#notification-div").delay(1000).fadeOut(500);
      }
