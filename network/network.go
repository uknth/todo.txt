/*
Takes care of all the network related functions
*/

package network

import (
	"bitbucket.org/pers3us/todo.txt/common"
	"bitbucket.org/pers3us/todo.txt/service"
	"fmt"
	"html/template"
	"log"
	"net/http"
)

var unsec *http.ServeMux

// main calls this to serve
func Server(port string) {
	unsec = http.NewServeMux()
	// Define Handlers
	unsec.HandleFunc("/", root)
	unsec.HandleFunc("/add", add)
	unsec.HandleFunc("/done", done)
	unsec.HandleFunc("/tasks", tasks)
	unsec.HandleFunc("/tags", tags)
	unsec.HandleFunc("/do", do)
	unsec.HandleFunc("/projects", projects)
	unsec.HandleFunc("/restore", restore)
	unsec.HandleFunc("/sync", sync)
	unsec.HandleFunc("/project", project)
	unsec.Handle("/css/", http.StripPrefix("/css/", http.FileServer(http.Dir("template/css"))))
	unsec.Handle("/js/", http.StripPrefix("/js/", http.FileServer(http.Dir("template/js"))))
	unsec.Handle("/img/", http.StripPrefix("/img/", http.FileServer(http.Dir("template/img"))))
	listen(port)
}

func listen(portnumber string) {

	err := http.ListenAndServe(":"+portnumber, unsec)
	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}
}

// HandleFunctions

// Just displays the bare bone, send the actual data using json
func root(w http.ResponseWriter, req *http.Request) {
	t := loadt()
	err := t.Execute(w, nil)
	common.Panic(err)
}

// adds a task
func add(w http.ResponseWriter, req *http.Request) {
	task := req.FormValue("task")
	if service.Add(task) {
		fmt.Fprintf(w, "OK")
		return
	} else {
		fmt.Fprintf(w, "ERROR")
	}
}

func do(w http.ResponseWriter, req *http.Request) {
	task := req.FormValue("id")
	log.Print("Recieved Request: Mark " + task + " as done.")
	if service.Do(task) {
		fmt.Fprintf(w, "OK")
		return
	} else {
		fmt.Fprintf(w, "ERROR")
	}
}

func restore(w http.ResponseWriter, req *http.Request) {
	task := req.FormValue("id")
	log.Print("Recieved Request: Mark " + task + " as not-done.")
	if service.Restore(task) {
		fmt.Fprintf(w, "OK")
		return
	} else {
		fmt.Fprintf(w, "ERROR")
	}
}

// marks a task as done
func done(w http.ResponseWriter, req *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	w.Write(service.Done())
	return
}

// lists the task
func tasks(w http.ResponseWriter, req *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	w.Write(service.Task())
	return
}

// lists the tags
func tags(w http.ResponseWriter, req *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	w.Write(service.Tags())
	return
}

// list the lists
func projects(w http.ResponseWriter, req *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	w.Write(service.Projects())
	return
}

func project(w http.ResponseWriter, req *http.Request) {
	project := req.FormValue("project")
	w.Header().Set("Content-Type", "application/json")
	w.Write(service.Project(project))
	return
}
func loadt() *template.Template {
	t, err := template.ParseFiles("template/index.html")
	common.Panic(err)
	return t
}

// Updates the objects with files.
func sync(w http.ResponseWriter, req *http.Request) {
	if service.Sync() {
		fmt.Fprintf(w, "OK")
	} else {
		fmt.Fprintf(w, "ERROR")
	}
}
